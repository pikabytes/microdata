<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <!-- CSRF Token -->
    <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
    <title>{{ config('app.name', 'Microdata') }}</title>
    <meta name="description" content="Prototype" />
    <meta name="keywords" content="software" />
    <meta name="author" content="Microdata" />
    <link rel="shortcut icon" href="{!!url('assets/global/img/logo2.png')!!}">
    <link rel="stylesheet" href="https://cdn.lineicons.com/1.0.1/LineIcons.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lalezar|Oxygen&display=swap" rel="stylesheet">
    <link href="{!!url('assets/global/css/bootstrap.min.css')!!}" rel="stylesheet">
    <link href="{!!url('assets/global/css/mdb.min.css')!!}" rel="stylesheet">
    <link href="{!!url('assets/global/css/style.css')!!}" rel="stylesheet">
    @yield('style')
</head>
<body>
<header>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img width="200" class="img-fluid" src="{!!url('assets/global/img/logo-blanco.png')!!}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto smooth-scroll">
                    <li class="nav-item active">
                        <a class="nav-link waves-effect waves-light" href="index.html">INICIO
                        <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="#certificaciones">CERTIFICACIONES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="#servicios">SERVICIOS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="#productos">PRODUCTOS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="#footer">CONTÁCTANOS</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <div class="box-text-header container mt-5 pt-5">
                <h1>SOMOS EL MEJOR EQUIPO DE</h1><h1 class="text-color"> DESARRROLLO</h1>
                <p>Somos una empresa dedicada al desarrollo de software y servicios de tecnologías de la información, estamos dentro de las mejores empresas de software de Arequipa, líder en el desarrollo de soluciones de software personalizadas, conformado por un equipo de profesionales con gran experiencia en ingeniería de software y administración. Estamos enfocados en proveer productos y servicios con calidad a plena satisfacción de nuestros clientes.</p>
            </div>
            </div>
            <div class="col-md-6">
            <div class="image-header"><img height="50" src="{!!url('assets/global/img/header.jpg')!!}" alt=""></div>
            </div>
        </div>
    </div>
    </section>
</header>

    @yield('header')


<!-- FOOTER -->
<footer id="footer" class="footer-area">
    <div class="footer-widget">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="footer-content text-center">
                        <a href="index.html"><img class="image-fluid" width="200" src="{!!url('assets/global/img/logo.png')!!}" alt="Logo"></a>
                        <p class="mt-">Si busca una empresa de software confiable y con experiencia, no espere más y contáctenos hoy mismo visite nuestra oficina principal o comuniquese a los siguientes teléfonos:</p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6 col-sm-7">
                    <div class="contact-box text-center mt-30">
                        <div class="contact-icon">
                            <i class="lni-map-marker"></i>
                        </div>
                        <div class="contact-content">
                            <h6 class="contact-title">Dirección</h6>
                            <p>Calle Sebastián Barranca #311 - 2do piso Urb. La Perla - Cercado, Arequipa</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-7">
                    <div class="contact-box text-center mt-30">
                        <div class="contact-icon">
                            <i class="lni-phone"></i>
                        </div>
                        <div class="contact-content">
                            <h6 class="contact-title">Telefono</h6>
                            <p>054-202063</p>
                            <p>957462994 - 959757304</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-7">
                    <div class="contact-box text-center mt-30">
                        <div class="contact-icon">
                            <i class="lni-envelope"></i>
                        </div>
                        <div class="contact-content">
                            <h6 class="contact-title">Email</h6>
                            <p>consultas@microdata.com.pe</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-form pt-30">
                        <form id="contact-form">
                            <div class="single-form">
                                <input type="text" name="name" placeholder="Nombre">
                            </div>
                            <div class="single-form">
                                <input type="email" name="email" placeholder="Email">
                            </div>
                            <div class="single-form">
                                <textarea name="message" placeholder="Escríbenos tu mensaje"></textarea>
                            </div>
                            <p class="form-message"></p>
                            <div class="single-form">
                                <button class="main-btn" type="submit">Enviar Mensaje</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6 pt-30">
                        <div class="fb-page pt-30" data-href="https://www.facebook.com/MicroDataArequipa/" data-tabs="timeline" data-width="600" data-height="130" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/MicroDataArequipa/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/MicroDataArequipa/">MicroData</a></blockquote></div>
                    </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright pb-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright-text text-center pt-20">
                        <p>Copyright © 2019. MicroData</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.3"></script>
<script type="text/javascript" src="<?php echo URL::asset('assets/global/scripts/jquery-3.4.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo URL::asset('assets/global/scripts/popper.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo URL::asset('assets/global/scripts/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo URL::asset('assets/global/scripts/mdb.min.js'); ?>"></script>
@yield('script')
</body>
</html>
