@extends('layouts.app')

@section('style')
    <link href="{!!url('assets/global/css/normalize.min.css')!!}" rel="stylesheet" type="text/css" />
    <link href="{!!url('assets/global/css/animate.min.css')!!}" rel="stylesheet" type="text/css" />
@endsection

@section('header')

<section id="mision-vision" class="mision-vision">
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="box-mision animated fadeIn" id="img-fadeIn3">
                            <h4><i class="lni-cup"></i><br>MISIÓN</h4>
                            <p>Crear soluciones de software que ayuden a nuestros clientes alcanzar sus objetivos de negocio.</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="animated fadeIn" id="img-fadeIn4">
                            <h4><i class="lni-stats-up"></i><br>VISIÓN</h4>
                            <p>Ofrecer e implementar servicios de software a escala global, convirtiéndonos en el socio estratégico de grandes organizaciones en diversos sectores.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="servicios" class="servicios">
    <div class="container">
        <h4 class="text-center py-4">SERVICIOS</h4>
        <div class="row">
            <div class="col-md-4 mb-4">
                <div class="card" style="box-shadow:none">
                    <div class="card-body text-center">
                        <div class="view">
                            <a href="#" class="icon">
                                <i class="lni-cloudnetwork"></i>
                            </a>
                        </div>
                        <h5 class="card-title">SERVICIOS DE COMPUTACIÓN EN LA NUBE</h5>
                        <p class="card-text">Ofrecemos servicios de computación en la nube como un servicio a través de Internet. Estos servicios abarcan desde ejecución de aplicaciones software, bases de datos, almacenamiento y escritorios virtuales. Contamos con proveedores de servicios con altos estándares de seguridad para su negocio.</p>
                        <a href="#" class="btn btn-outline-green btn-md waves-effect">LEER MÁS</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <div class="card-body text-center">
                        <div class="view">
                            <a href="#" class="icon">
                                <i class="lni-cog"></i>
                            </a>
                        </div>
                        <h5 class="card-title">DESARROLLO DE SOFTWARE</h5>
                        <p class="card-text">Contamos con un sólido equipo humano en Ingeniería de Software que sigue un proceso sistemático, disciplinado y cuantificable para el desarrollo, operación y mantenimiento de software. Gestión de proyectos bajo el enfoque PMI © PMBOK e implementación de software bajo el enfoque ISO/IEC 29110..</p>
                        <a id="servicio2" href="#desarrolloSoftware" class="btn btn-outline-blue btn-md waves-effect">LEER MÁS</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card" style="box-shadow:none">
                    <div class="card-body text-center">
                        <div class="view">
                            <a href="#" class="icon">
                                <i class="lni-blackboard"></i>
                            </a>
                        </div>
                        <h5 class="card-title">MODELADO DE PROCESOS DE NEGOCIO</h5>
                        <p class="card-text">La Gestión de Procesos de Negocio es una metodología corporativa, cuyo objetivo es mejorar el desempeño y la optimización de los procesos de negocio de una organización, a través de la gestión de los procesos que se deben diseñar, modelar, organizar, documentar y optimizar de forma continua</p>
                        <a href="#" class="btn btn-outline-green btn-md waves-effect">LEER MÁS</a>
                    </div>
                </div>
            </div>
            <!--POPUP DESARROLLO DE SOFTWARE-->
            <div id="desarrolloSoftware">
                <!-- THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID -->
                <div  id="btn-close-modal" class="close-desarrolloSoftware">
                    <a href="#" class="icon">
                        <i class="lni-close"></i>
                    </a>
                </div>
                <div class="modal-content">
                    <p class="text-center">En MICRODATA empleamos los siguientes procesos de desarrollo de software:</p>
                    <div class="text-center">
                        <h2 class="text-center">PROCESO DE GESTIÓN DEL PROYECTO - GP</h2>
                        <p class="text-center">El propósito del proceso de Gestión de Proyecto es establecer y llevar a cabo de manera sistemática las Tareas de un proyecto de implementación de Software, que permitan cumplir con los Objetivos del proyecto en calidad, tiempo, costos esperados.</p>
                        <img src="{!!url('assets/global/img/servicios/1.jpg')!!}" class="img-fluid" alt="">
                    </div>
                    <div class="text-center">
                        <h2 class="text-center">PROCESO DE IMPLEMENTACIÓN DE SOFTWARE - IS</h2>
                        <p class="text-center">El propósito del proceso de Implementación de Software es la realización sistemática de las actividades de análisis, diseño, construcción, integración y pruebas para los productos Software, nuevos o modificados, de acuerdo a los requisitos especificados.</p>
                        <img src="{!!url('assets/global/img/servicios/1.jpg')!!}" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
            <!--POPUP DESARROLLO DE SOFTWARE-->
        </div>
    </div>
</section>

<section id="certificaciones" class="certificaciones">
        <div class="container">
            <h4 class="text-center py-4">CERTIFICACIONES</h4>
            <div class="row">
                <div class="mx-auto d-block">
                    <ul class="list-group list-group-horizontal">
                        <li class="list-group-item"><img src="{!!url('assets/global/img/certificaciones/ISO.png')!!}"></li>
                        <li class="list-group-item"><img src="{!!url('assets/global/img/certificaciones/logo-calidad.png')!!}"></li>
                        <li class="list-group-item"><img src="{!!url('assets/global/img/certificaciones/moprosoft.png')!!}"></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<section class="productos" id="productos" class="feature-content section-padding">
    <div class="container">
        <h4 class="text-center py-4">PRODUCTOS</h4>
        <div class="row">
            <div class="col-lg-6 col-sm-12 col-xs-12">
                <div class="row text-center d-flex justify-content-center">
                    <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.1s" data-wow-offset="0" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: fadeInUp;">
                        <div class="single_feature single_feature_mt">
                            <i class="lni-ambulance"></i>
                            <h4>MEDIMERCADO</h4>
                            <span></span>
                            <p>Medimercado, es una plataforma virtual que facilita las negociaciones de compra y venta de medicamentos entre boticas.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.2s" data-wow-offset="0" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeInUp;">
                        <div class="single_feature single_feature_bg">
                            <i class="lni-stethoscope"></i>
                            <h4>SMARTSALUD</h4>
                            <span></span>
                            <p>SMARTSalud es un producto innovador que integra tecnologías de recolección de datos de pacientes a través de sensores médicos y dispositivos.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0" style="visibility: visible; animation-duration: 1s; animation-delay: 0.3s; animation-name: fadeInUp;">
                        <div class="single_feature">
                            <i class="lni-seo-monitoring"></i>
                            <h4>SISPLA</h4>
                            <span></span>
                            <p>El Sistema de Planillas ha sido diseñado para que de una manera ágil el asistente de planillas pueda llevar el control y cálculo de todos los procesos involucrados.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 col-xs-12">
                <div class="feature_img">
                    <img src="{!!url('assets/global/img/feature.png')!!}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section id="counter" class="section-padding casos-de-exito">
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-12 col-md-12 col-xs-12">
                    <h4 class="text-center py-4 white-text">DATOS IMPORTANTES</h4>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="counter-box wow fadeInUp animated" data-wow-delay="0.2s" style="visibility: visible;-webkit-animation-delay: 0.2s; -moz-animation-delay: 0.2s; animation-delay: 0.2s;">
                            <div class="icon-o"><i class="lni-baloon"></i></div>
                            <div class="fact-count">
                                <h3><span class="counter">+ 50</span></h3>
                                <p>PROYECTOS TERMINADOS</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="counter-box wow fadeInUp animated" data-wow-delay="0.4s" style="visibility: visible;-webkit-animation-delay: 0.4s; -moz-animation-delay: 0.4s; animation-delay: 0.4s;">
                            <div class="icon-o"><i class="lni-emoji-cool"></i></div>
                            <div class="fact-count">
                                <h3><span class="counter">+ 20</span></h3>
                                <p>CLIENTES SATISFECHOS</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="counter-box wow fadeInUp animated" data-wow-delay="0.6s" style="visibility: visible;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                            <div class="icon-o"><i class="lni-code-alt"></i></div>
                            <div class="fact-count">
                                <h3><span class="counter">+ 5000</span></h3>
                                <p>KLOC GENERADAS</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-xs-12">
                        <div class="counter-box wow fadeInUp animated" data-wow-delay="0.8s" style="visibility: visible;-webkit-animation-delay: 0.8s; -moz-animation-delay: 0.8s; animation-delay: 0.8s;">
                            <div class="icon-o"><i class="lni-star"></i></div>
                            <div class="fact-count">
                                <h3><span class="counter">+ 15</span></h3>
                                <p>AÑOS DE EXPERIENCIA</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonios" id="testimonios">
    <div class="container">
        <h4 class="text-uppercase text-center py-4">Nuestros clientes y entidades asociadas</h4>
        <div class="wrapper-carousel-fix">
            <div id="carousel-example-1" class="carousel no-flex testimonial-carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial">
                            <div class="avatar mx-auto mb-4">
                                <img src="{!!url('assets/global/img/partnes/estilos-logo.jpg')!!}" class="img-fluid">
                            </div>
                            <p>
                                <i class="lni-play"></i> Estimados amigos de MICRODATA, un verdadero gusto de trabajar con ustedes en varios proyectos muy bien llevados a tiempo y con toda la seriedad del caso, para Estilos es importante la calidad del trabajo y las entregas a tiempo. Un abrazo y que sigan los éxitos.
                            </p>
                            <h4 class="font-weight-bold">Ing. Paúl Pérez Quintanilla</h4>
                            <h6 class="font-weight-bold my-3">Gerente de Tecnología de la Información - ESTILOS</h6>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial">
                            <div class="avatar mx-auto mb-4">
                                <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg" class="img-fluid">
                            </div>
                            <p>
                                <i class="lni-play"></i> La experiencia de haber trabajado con ustedes, ha permitido aprender que son un equipo con grandes valores y principios, no sólo buscan la excelencia en el campo tecnológico de su competencia, fundamentalmente buscan permanentemente contribuir a mejorar la vida de las personas usuarias de sus servicios. Una empresa joven con un futuro lleno de compromisos al servicio de los demás, es una empresa con la que siempre vale la pena trabajar.</p>
                            <h4 class="font-weight-bold">DR. Braulio Cuba Corrido</h4>
                            <h6 class="font-weight-bold my-3">Gerente General - B&M CONSULTORES EIRL</h6>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial">
                            <div class="avatar mx-auto mb-4">
                                <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(3).jpg" class="img-fluid">
                            </div>
                            <p>
                                <i class="lni-play"></i> Como consultora externa de MICRODATA he visto de cerca el trabajo realizado, el profesionalismo y sobre todo los resultados obtenidos en cada proyecto desde mejora de procesos hasta desarrollo de software. Microdata es un aliado estratégico de las empresas que quieren desarrollar ventaja competetiva. Quiero expresar mis sinceras felicitaciones al equipo de Microdata que está generando grandes cambios en el sur del pais.</p>
                            <h4 class="font-weight-bold">ING. Elizabeth Vidal Duarte</h4>
                            <h6 class="font-weight-bold my-3">CONSULTORA BPM</h6>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial">
                            <div class="avatar mx-auto mb-4">
                                <img src="{!!url('assets/global/img/partnes/logo-colegiodeingenieros.png')!!}" class="img-fluid">
                            </div>
                            <p>
                                <i class="lni-play"></i> Quiero en esta oportunidad enviarles mis felicitaciones a todo el equipo de profesionales que forman la familia Microdata por su labor interrumpida sacando adelante la industria del software en nuestro país, en todo este tiempo los he venido siguiendo de cerca y me enorgullece que una empresa de Arequipa este trabajando con estándares globales de calidad de proceso y calidad de producto software, alcanzando sendas certificaciones internacionales lo cual es una ventaja competitiva y los posiciona como una empresa referente en el medio.</p>
                            <h4 class="font-weight-bold">Mg. Edward Zárate Carlos</h4>
                            <h6 class="font-weight-bold my-3">Presidente del Capítulo de Ingeniería de Sistemas Colegio de Ingenieros del Perú – Consejo Departamental de Arequipa<br><br>Gerente General PM Software & Consulting SAC</h6>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev left carousel-control" href="#carousel-example-1" role="button" data-slide="prev">
                    <i class="lni-chevron-left"></i>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next right carousel-control" href="#carousel-example-1" role="button" data-slide="next">
                    <i class="lni-chevron-right"></i>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="partnes" id="partnes">
    <div class="container">
        <div class="row">
            <div class="mx-auto d-block">
                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item"><img src="{!!url('assets/global/img/partnes/estilos-logo.jpg')!!}"></li>
                    <li class="list-group-item"><img src="{!!url('assets/global/img/partnes/logo-tisur.svg')!!}"></li>
                    <li class="list-group-item"><img src="{!!url('assets/global/img/partnes/logo-seal.png')!!}"></li>
                    <li class="list-group-item"><img src="{!!url('assets/global/img/partnes/logo-cajaarequipa.png')!!}"></li>
                    <li class="list-group-item"><img src="{!!url('assets/global/img/partnes/logo-pacífico.png')!!}"></li>
                </ul>
            </div>
        </div>
    </div>
</section>
    @section('script')
    <script type="text/javascript" src="<?php echo URL::asset('assets/global/scripts/animatedModal.min.js'); ?>"></script>
    <script>
        //demo 01
        $("#servicio2").animatedModal();
    </script>
    @endsection

@endsection
